import com.google.gson.JsonElement;
import org.junit.Test;

import java.io.File;
import java.util.HashMap;

import static org.junit.Assert.assertTrue;

public class JSONReaderTests {

    JSONreader jsoNreader = new JSONreader();
    ConsoleWriter consoleWriter = new ConsoleWriter();

    @Test
    public void jsonObjectTest(){
        checkAmountOfValuesInJson("1.txt",
                "name", 6, "5", 3);

    }

    /*Comment to this Test. There are 2 names and both are "qwe".
    * But they are inside one Essence and for me it is one name and its
    * duplicate, not two different names...
    *
    * If you don't agree with me, we can change elementCounter method, but
    * I would like not to change it.
    * */
    @Test
    public void jsonObjectTest2(){
        checkAmountOfValuesInJson("2.txt",
                "name", 1, "qwe", 1);
    }

    @Test
    public void jsonArrayTest(){
        checkAmountOfValuesInJson("3.txt",
                "name", 3, "qwe", 2);
    }

    @Test
    public void jsonArrayTest2(){
        checkAmountOfValuesInJson("4.txt",
                "name", 3, "qwe", 2);
    }

    private void checkAmountOfValuesInJson(String path, String key, Integer keyAmount, String value, Integer valueAmount){
        //take absolute path
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(path).getFile());

        //check amounts
        JsonElement el = JSONreader.fileToJson(file.getAbsolutePath());
        HashMap<String, Integer> counts = ConsoleWriter.createHashMap();
        counts = JSONreader.elementCounter(key, value, el, counts);
        assertTrue("Incorrect amount of keys " + counts.get("first"), counts.get("first").equals(keyAmount));
        assertTrue("Incorrect amount of values" + counts.get("second"), counts.get("second").equals(valueAmount));
    }
}
