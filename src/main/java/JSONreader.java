import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class JSONreader {

    //Standard convert to the JSON format
    public static JsonElement fileToJson(String path){
        try{
            FileReader reader = new FileReader(path);
            JsonParser parser = new JsonParser();
            return parser.parse(reader);
        }
        catch (IOException e){
           e.printStackTrace();
        }
        return null;
    }

    //We expect that JSON will be of Array type or Object type. Then we check all the fields by recursion
    public static HashMap<String, Integer> elementCounter(String key, String value, JsonElement jsonElement, HashMap<String, Integer> hash){
        //We expect that response will be an Array or Object styled. But if it is an Array, it will have Objects inside
        //So we take each array node and check like new JSON
        if(jsonElement.isJsonArray()){
            for (JsonElement jsonElement1 : jsonElement.getAsJsonArray()) {
                elementCounter(key, value, jsonElement1, hash);
            }
        }
        //If we receive an Object, we will put it in a Set with Keys as String and Values as JSONObject
        else if(jsonElement.isJsonObject()){
            Set<Map.Entry<String, JsonElement>> entrySet = jsonElement.getAsJsonObject().entrySet();
            //In each node we analyze if the key equals to what we need
            for (Map.Entry<String, JsonElement> entry : entrySet) {
                String key1 = entry.getKey();
                if (key1.equals(key)) {
                    //If Key is correct, we will put it into HashMap and increment counter of the Key
                    hash.put("first", hash.get("first") + 1);
                    if(entry.getValue().toString().replaceAll("\"", "").equals(value))
                        //If Value of the is correct, we will put it into HashMap and increment counter of the Value
                        hash.put("second", hash.get("second") + 1);
                    /*
                    But sometimes value can be inside the array of values, so we check if the value is an array and
                    if it as, we check is there a value, that we need.
                     */
                    else if(entry.getValue().isJsonArray()){
                        for (JsonElement element: entry.getValue().getAsJsonArray()) {
                            if(element.toString().replaceAll("\"", "").equals(value))
                                hash.put("second", hash.get("second") + 1);
                        }
                    }
                }
                elementCounter(key, value, entry.getValue(), hash);
            }
        }
        return hash;
    }
}
