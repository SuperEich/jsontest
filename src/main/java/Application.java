import com.google.gson.JsonElement;
import java.util.HashMap;


public class Application {

    JSONreader jsoNreader = new JSONreader();
    ConsoleWriter consoleWriter = new ConsoleWriter();

    public static void main(String[] args){

        //We convert file to JSON
        JsonElement el = JSONreader.fileToJson(args[0]);

        //We create HashMap of needed format
        HashMap<String, Integer> counts = ConsoleWriter.createHashMap();
        //We get HashMap of needed information: count of keys, count of keys with specific value
        counts = JSONreader.elementCounter(args[1], args[2], el, counts);
        //We put information from HashMap to console
       ConsoleWriter.standardConsoleWriter(counts, args[1], args[2]);
    }

}
