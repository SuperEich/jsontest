import java.util.HashMap;

public class ConsoleWriter {

    //We put needed information to the console
    public static void standardConsoleWriter(HashMap<String, Integer> counts, String key, String value){
        System.out.println("Found " + counts.get("first") + " objects with field " + key);
        System.out.println("Found " + counts.get("second") + " objects where " + key + " equals  " + value);
    }

    //We need such method for standardConsoleWriter method
    public static HashMap<String, Integer> createHashMap(){
        HashMap<String, Integer> counts = new HashMap<>();
        counts.put("first", 0);
        counts.put("second", 0);
        return counts;
    }
}
